// @flow
declare type PKPlayerOptionsObject = {
  log?: PKLogConfigObject,
  playback?: PKPlaybackConfigObject,
  streaming?: PKStreamingConfigObject,
  sources?: PKSourcesConfigObject,
  session?: PKSessionConfigObject,
  network?: PKNetworkConfigObject,
  customLabels?: PKCustomLabelsConfigObject
};
